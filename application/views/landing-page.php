<!doctype html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Bootstrap demo</title>


      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

      <link rel="stylesheet" href="./css/bootstrap.css">
      <link rel="stylesheet" href="./css//bootstrap-grid.css">
      <link rel="stylesheet" href="./libs/fontawesome/css/fontawesome.css">
      <link rel="stylesheet" href="./libs/fontawesome/css/fontawesome.min.css">
      <link rel="stylesheet" href="./libs/fontawesome/css/all.css">
      <link rel="stylesheet" href="./css/style.css">
  </head>
  <body>

        <header>
          <div class="navbar navbar fixed-top navbar-expand-lg navbar-light bg-light py-3 shadow-sm sticky-top">
          <div class="container">
              <!-- <nav class="navbar navbar-expand-lg navbar-light bg-light"> -->
                  <a class="navbar-brand" href="#">
                      <img src="https://unsplash.com/photos/LXI5kqCdEcE" alt="" width="100">
                  </a>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                  </button>
              
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                      <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">Produk</a>
                    </li>
                    <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                        Contact
                      </a>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">Developer</a>
                        <a class="dropdown-item" href="#">IT Helpdesk</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                      </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Merchandise</a>
                      </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">A bout</a>
                      </li>
                  </ul>
                  <form class="form-inline my-2 my-lg-0">
                    <button class="btn btn-outline-secondary my-2 my-sm-0 mx-2" type="submit">Sign Up</button>
                    <button class="btn btn-secondary  my-2 my-sm-0" type="submit">Start Solution</button>
                    
                  </form>
                  
              </div>
            
          </header>

          <section class="jumbotron px-4 py-4">
                <div class="row align-items-center">
                    <div class="col-sm ml-4 ">
                        <h1>Solve problems quickly</h1>
                        <p>Safely build computer equipment and comfortable</p>

                        <a href="#" class="btn btn-secondary btn-lg my-2 my-sm-0 mr-2" type="submit">Start Solution</a>
                        <a href="#" class="text-muted"><i class=" text-muted fa-regular fa-circle-play fa-lg mr-2 "></i>How it work</a>
                    </div>
                    <div class="col-sm">
                        <img src="" class="img-fluid" alt="">
                        <div class="container">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                    <div class="carousel-inner">
                        <?php foreach($hero as $key => $rows): ?>
                          <div class="carousel-item <?php echo ($key == 0 ? 'active': '') ?>">
                            <img class="d-block w-100" src="<?php echo $rows['file_foto'] ?>"  alt="First Slide">
                            <div class="carousel-caption d-none d-md-block">
                              <h5><?php echo $rows['label']?></h5>
                              <p><?php echo $rows['description']?>.</p>
                            </div>
                          </div>
                        <?php endforeach; ?>
                    </div>
                      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                </div>
                </div>
                </div>
          </section>

            
        <!-- Option 1: Bootstrap Bundle with Popper -->
            <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
            <script>
              $(document).ready(function(){
                $('.carousel').carousel({
                  interval: 2000
                })
              })
            </script>

            <script src="./js/jquery-3.6.1.js"></script>
            <script src="./js/bootstrap.js"></script>
            <script src="./libs/fontawesome/js/fontawesome.js"></script>
            <script src="./libs/fontawesome/js/all.js"></script>
  </body>
</html>