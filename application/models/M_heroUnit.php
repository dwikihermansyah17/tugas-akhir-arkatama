<?php defined('BASEPATH') OR exit ('no access allowed');
class M_heroUnit extends CI_MODEL {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function getHero(){
        $this->db->select("*");
        $this->db->from('hero_unit');
        $this->db->where('status_persetujuan','sudah diterima');
        $get = $this->db->get();
        return $get->result_array();
    }
}
?>